# Setup cluster and training environment

## Step 1: Setup Cloud Shell environment

In the shell at the bottom, populate the configuration variables that have been sent to you by e-mail:

```
gcloud container clusters get-credentials --zone=europe-west4-a --project ses-environment <<your cluster name, userXX>>
```

## Step 2: Clone training material

```
git clone https://gitlab.com/innoq/container-e2e-demo/container-training.git
cd container-training/
```

## Step 3 (optional): Install K9s

```
curl -LJO https://github.com/derailed/k9s/releases/download/v0.32.5/k9s_Linux_amd64.tar.gz && tar -xvf k9s_Linux_amd64.tar.gz
```

Open K9s via 

```
./k9s
```