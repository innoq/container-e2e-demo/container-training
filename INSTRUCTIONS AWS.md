# Setup cluster and training environment

## Step 1: Setup Cloud9 development environment

In the shell at the bottom, populate the configuration variables that have been sent to you by e-mail:

```
export AWS_ACCESS_KEY_ID="<AccessKeyId>"
export AWS_SECRET_ACCESS_KEY="<SecretAccessKey>"
...
```

## Step 2: Install basic tools
```
cd container-training/
sudo ./cloud9/install-tools.sh
./cloud9/initialize-environment.sh ${USER_NAME}
```

## Step 3: Install NGINX ingress controller
```
./cloud9/nginx/deploy.sh
```

## Step 4: Install Cluster Autoscaler
```
./cloud9/cluster-autoscaler/deploy.sh
```

# Install Kubernetes Dashboard

## Step 1: Install external-dns
```
./cloud9/external-dns/deploy.sh
```

## Step 2: Install cert-manager
```
./cloud9/cert-manager/deploy.sh
```

## Step 3: Install Kubernetes Dashboard
```
./cloud9/dashboard/deploy.sh
```

## Step 4: Restart cert-manager

```
kubectl delete pod -n cert-manager -l app=cert-manager
# kubectl logs -n cert-manager -l app=cert-manager -f
```

## Step 5: Login to dashboard

Run the following commands in shell:

```
SECRET_NAME=$(kubectl get sa kubernetes-dashboard -o jsonpath="{.secrets[0].name}")
TOKEN=$(kubectl get secret ${SECRET_NAME} -o jsonpath="{.data.token}" | base64 --decode)
echo $TOKEN
```

Copy the token printed on the screen into the clipboard and open https://dashboard.${USER_NAME}.trainings-on-aws.ses.innoq.io in your browser.


# CI/CD demo preparation

## Step 3: Install Postgres database for microservices demo
```
./cloud9/database/deploy.sh
```

# Install monitoring and logging tools

## Step 1: Install Keycloak
```
./cloud9/keycloak/deploy.sh
```

## Step 2: Install Prometheus
```
./cloud9/prometheus/deploy.sh
```

## Step 3: Install Grafana
```
./cloud9/grafana/deploy.sh
```

## Step 4: Install Elasticsearch and Kibana
```
./cloud9/elasticsearch-kibana/deploy.sh
```

# Final steps at the end of the training

## Clean up Kubernetes cluster resources

```
./cloud9/delete-cluster-resources.sh
```