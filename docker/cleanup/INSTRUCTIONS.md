# Cleanup

```bash
docker ps
```
```bash
docker container rm -f $(docker container ls -aq)
```
```bash
docker system prune -a -f --volumes
```
