# Multi Stage

```bash
cd ../multi-stage-old
```
```bash
docker build -t <NAME>:<TAG> .(e.g. training:second)
```
```bash
docker history <NAME>:<TAG>
```
```bash
docker inspect <NAME>:<TAG>
```