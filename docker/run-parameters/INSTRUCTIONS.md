# Run Parameters

```bash
cd ../run-parameters
```
```bash
docker build -t training:figlet-v1 .
```
```bash
docker run --rm training:figlet-v1 HELLO!
```
```bash
docker run --name fig1 training:figlet-v1 DOCKER!
```
```bash
docker inspect -f "{{ .State.Status }}" fig1
```
```bash
docker ps [-a]
```
```bash
docker run --name fig1 training:figlet-v1 DOCKER AGAIN!
```
### ⚡️!
```bash
docker run -d -p 8080:80 --name demo1 training:first
```
```bash
docker exec -it demo1 bash
  root@container: nano index.html
  root@container: apt-get install -y nano
  root@container: nano index.html (*1)
  root@container: exit
docker rm -f demo1
```
(*1): Change the content of the file and save the changes via Ctrl + X, Y, ENTER

### Open the web preview in GCP cloud console
