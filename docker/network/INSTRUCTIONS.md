# Docker Network

```bash
docker network create test-net
```
```bash
docker run -d --name web --net test-net nginx:stable
```
```bash
docker run -it --net test-net busybox
  root@container: wget -O - http://web
  root@container: wget -O - http://www.google.com

    In 2nd terminal: docker network inspect test-net

  root@container: exit
```
```bash
docker rm -f web
```
```bash
docker run -d --net test-net --net-alias web nginxdemos/hello(run 3x)
```
```bash
docker run -it --net test-net busybox
  root@container: wget -O - http://web -q | grep "<span>172"
  root@container: exit
```
```bash
docker rm $(docker stop $(docker ps -a -q --filter="ancestor=nginxdemos/hello"))
```
```bash
docker network remove test-net
```