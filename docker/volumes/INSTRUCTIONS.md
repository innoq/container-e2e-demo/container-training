# Docker Volumes

```bash
docker run -d -p 8080:80 --name web -v shared-volume:/var/www/html training:first
```
```bash
docker exec -it web bash
  root@container: echo "New Content" > /var/www/html/index.html
  root@container: exit
```
```bash
docker rm -f web
```
```bash
docker volume ls
```
```bash
docker run -d -p 8080:80 --name web -v shared-volume:/var/www/html training:first
```
```bash
docker rm -f web
```
