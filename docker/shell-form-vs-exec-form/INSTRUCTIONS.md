# Shell form vs. exec form

```bash
cd ../shell-form-vs-exec-form
```
```bash
./build-and-run-exec-form.sh
```

### Run in 2nd terminal:

```bash
docker stop exec-form
```
### How long does it take until the container is stopped? What is the ouput in the first terminal?
```bash
./build-and-run-shell-form.sh
```

### Run in 2nd terminal:

```bash
docker stop shell-form
```
### How long does it take until the container is stopped? What is the ouput in the first terminal?