#!/bin/bash

docker rm -f shell-form
docker build -t shell-form:1 -f shell-form.Dockerfile .
docker run -d --name shell-form shell-form:1
docker logs -f shell-form