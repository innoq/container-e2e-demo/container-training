#!/bin/bash

trap "echo 'Got SIGINT signal, terminating!'; exit 0" INT
trap "echo 'Got SIGTERM signal, terminating!'; exit 0" TERM

echo "Looping..."

for i in {1..100}
do
    echo "Hello $i!"
    sleep 1
done

echo "Starting nginx..."

exec nginx -g 'daemon off;'