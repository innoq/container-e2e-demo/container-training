FROM ubuntu:18.04

RUN apt-get update ; apt-get dist-upgrade -y -qq 

RUN apt-get install -y -qq nginx

ADD entrypoint.sh /entrypoint.sh

CMD /entrypoint.sh