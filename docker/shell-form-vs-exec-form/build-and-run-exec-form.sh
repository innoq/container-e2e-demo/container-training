#!/bin/bash

docker rm -f exec-form
docker build -t exec-form:1 -f exec-form.Dockerfile .
docker run -d --name exec-form exec-form:1
docker logs -f exec-form