# Build Image

```bash
cd container-training
cd docker/basic
```
```bash
docker build .
```
### IMAGE_ID = last line of build output
```bash
docker history <IMAGE_ID>
```
```bash
docker tag <IMAGE_ID> training:first
```
```bash
docker history training:first
```
```bash
docker build -t training:first .
```
### (faster, right?)
```bash
docker build --no-cache -t training:first . 
```
### (same (slow) speed as the first command?)