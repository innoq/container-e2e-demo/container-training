# Helm

```bash
helm template --set echo=YEAH .
```
```bash
helm install echoserver  --set echo=YEAH .
```

Validate created resources and check if you can reach your deployment from the internet.

```bash
helm uninstall echoserver
```