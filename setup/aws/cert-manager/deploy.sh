#!/bin/bash

SCRIPT_FOLDER=$(realpath $0)
SCRIPT_FOLDER=$(dirname ${SCRIPT_FOLDER})

kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v0.14.1/cert-manager.crds.yaml
kubectl create namespace cert-manager
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm upgrade --install cert-manager jetstack/cert-manager -f ${SCRIPT_FOLDER}/values.yaml \
    --set serviceAccount.annotations."eks\.amazonaws\.com/role-arn"="${CERT_MANAGER_SA_ROLE_ARN}" \
    --namespace cert-manager --version v0.14.1

echo "Please wait 30 seconds..."
sleep 30

export HOSTED_ZONE_ID=$(aws route53 list-hosted-zones-by-name --output text --dns-name "${DNS_DOMAIN}." --query "HostedZones[0].Id")

cat ${SCRIPT_FOLDER}/cert-manager-cluster-issuer-staging.yaml | envsubst | kubectl apply -f -