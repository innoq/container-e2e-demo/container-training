#!/bin/bash

SCRIPT_FOLDER=$(realpath $0)
SCRIPT_FOLDER=$(dirname ${SCRIPT_FOLDER})

helm repo add kubernetes-dashboard https://kubernetes.github.io/dashboard/
helm repo update

helm upgrade --install dashboard -f ${SCRIPT_FOLDER}/values.yaml \
    --set ingress.hosts[0]=dashboard.${USER_NAME}.${DNS_DOMAIN} \
    --set ingress.tls[0].hosts[0]=dashboard.${USER_NAME}.${DNS_DOMAIN} \
    kubernetes-dashboard/kubernetes-dashboard
