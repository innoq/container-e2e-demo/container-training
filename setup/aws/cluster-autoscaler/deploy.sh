#!/bin/bash

SCRIPT_FOLDER=$(realpath $0)
SCRIPT_FOLDER=$(dirname ${SCRIPT_FOLDER})

# https://docs.aws.amazon.com/de_de/eks/latest/userguide/cluster-autoscaler.html

curl -s https://raw.githubusercontent.com/kubernetes/autoscaler/master/cluster-autoscaler/cloudprovider/aws/examples/cluster-autoscaler-autodiscover.yaml \
    | sed "s/<YOUR CLUSTER NAME>/${USER_NAME}/g" | kubectl apply -f -

kubectl -n kube-system annotate deployment.apps/cluster-autoscaler cluster-autoscaler.kubernetes.io/safe-to-evict="false"
kubectl -n kube-system set image deployment.apps/cluster-autoscaler cluster-autoscaler=us.gcr.io/k8s-artifacts-prod/autoscaling/cluster-autoscaler:v1.16.5