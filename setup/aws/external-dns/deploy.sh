#!/bin/bash

SCRIPT_FOLDER=$(realpath $0)
SCRIPT_FOLDER=$(dirname ${SCRIPT_FOLDER})

helm repo add bitnami https://charts.bitnami.com/bitnami

HOSTED_ZONE_ID=$(aws route53 list-hosted-zones-by-name --output text --dns-name "${DNS_DOMAIN}." --query "HostedZones[0].Id")

helm upgrade --install external-dns -f ${SCRIPT_FOLDER}/values.yaml \
    --set txtOwnerId=${HOSTED_ZONE_ID} \
    --set domainFilters[0]=${DNS_DOMAIN} \
    --set serviceAccount.annotations."eks\.amazonaws\.com/role-arn"="${EXTERNAL_DNS_SA_ROLE_ARN}" \
    bitnami/external-dns
