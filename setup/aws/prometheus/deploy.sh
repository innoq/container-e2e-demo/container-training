#!/bin/bash

SCRIPT_FOLDER=$(realpath $0)
SCRIPT_FOLDER=$(dirname ${SCRIPT_FOLDER})

helm upgrade --install -f ${SCRIPT_FOLDER}/prometheus-values.yaml prometheus stable/prometheus
helm upgrade --install -f ${SCRIPT_FOLDER}/prometheus-oauth2-proxy-values.yaml \
    --set ingress.host=prometheus.${USER_NAME}.${DNS_DOMAIN} \
    --set keycloak.baseUrl=https://keycloak.${USER_NAME}.${DNS_DOMAIN} \
    prometheus-oauth2-proxy ./oauth2-proxy