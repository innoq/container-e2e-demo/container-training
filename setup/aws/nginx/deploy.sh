#!/bin/bash

SCRIPT_FOLDER=$(realpath $0)
SCRIPT_FOLDER=$(dirname ${SCRIPT_FOLDER})

helm repo add stable https://kubernetes-charts.storage.googleapis.com
helm repo update

helm upgrade --install nginx -f ${SCRIPT_FOLDER}/values.yaml stable/nginx-ingress