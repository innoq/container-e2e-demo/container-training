#!/bin/bash

if [ "$1" == "" ]; then
    echo "Please specify your user name, e.g. max-mustermann. Usage: ./initialize-environment.sh <USER NAME>"
    exit 1
fi

aws eks update-kubeconfig --name $1

kubectl get svc