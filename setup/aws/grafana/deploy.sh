#!/bin/bash

SCRIPT_FOLDER=$(realpath $0)
SCRIPT_FOLDER=$(dirname ${SCRIPT_FOLDER})

cat ${SCRIPT_FOLDER}/values.yaml | envsubst > ${SCRIPT_FOLDER}/values-substituted.yaml
helm upgrade --install -f ${SCRIPT_FOLDER}/values-substituted.yaml grafana stable/grafana
rm -f values-substituted

echo "Grafana user: admin"
echo "Password: "
kubectl get secret --namespace default grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo

# TODO: add http://prometheus-server:80 as data source
# TODO: import dashboard 6417
# TODO: create a new dashboard with teh metric "service_order_total"