#!/bin/bash

SCRIPT_FOLDER=$(realpath $0)
SCRIPT_FOLDER=$(dirname ${SCRIPT_FOLDER})

helm repo add elastic https://helm.elastic.co
helm repo update

helm upgrade --install elasticsearch elastic/elasticsearch
helm upgrade --install kibana elastic/kibana
helm upgrade --install -f ${SCRIPT_FOLDER}/kibana-oauth2-proxy-values.yaml \
    --set ingress.host=kibana.${USER_NAME}.${DNS_DOMAIN} \
    --set keycloak.baseUrl=https://keycloak.${USER_NAME}.${DNS_DOMAIN} \
    kibana-oauth2-proxy ./oauth2-proxy