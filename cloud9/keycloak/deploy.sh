#!/bin/bash

SCRIPT_FOLDER=$(realpath $0)
SCRIPT_FOLDER=$(dirname ${SCRIPT_FOLDER})

export KEYCLOAK_BASE_URL=${USER_NAME}.${DNS_DOMAIN}
cat ${SCRIPT_FOLDER}/container-e2e-demo-realm.json | envsubst '${KEYCLOAK_BASE_URL}' > ${SCRIPT_FOLDER}/realm.json

kubectl create secret generic realm-secret --from-file=${SCRIPT_FOLDER}/realm.json
rm -f ${SCRIPT_FOLDER}/realm.json

helm repo add codecentric https://codecentric.github.io/helm-charts

cat ${SCRIPT_FOLDER}/values.yaml | envsubst > ${SCRIPT_FOLDER}/values-substituted.yaml
helm upgrade --install -f ${SCRIPT_FOLDER}/values-substituted.yaml keycloak codecentric/keycloak
rm -f values-substituted

echo "Keycloak user: keycloak"
echo "Password: "
kubectl get secret --namespace default keycloak-http -o jsonpath="{.data.password}" | base64 --decode; echo

# TODO: create a user "demouser1" with password "demopw1" and the role mapping "grafana-admins"