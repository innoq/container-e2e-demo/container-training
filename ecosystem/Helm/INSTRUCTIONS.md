# Helm

```bash
helm template .
```
```bash
helm install bluegreen .
```
```bash
kubectl run curl --image=radial/busyboxplus:curl -i --rm --tty
  root@container: curl http://bluegreen-service
  root@container: exit
```
```bash
helm upgrade bluegreen --set service.color=green --set nginx.version=1.21 .
```
```bash
kubectl run curl --image=radial/busyboxplus:curl -i --rm --tty
  root@container: curl http://bluegreen-service
  root@container: exit
```
```bash
helm rollback bluegreen
```
```bash
helm status bluegreen
```
```bash
helm history bluegreen
```
```bash
helm get all bluegreen --revision 2
```
```bash
kubectl run curl --image=radial/busyboxplus:curl -i --rm --tty
  root@container: curl http://bluegreen-service
  root@container: exit
```
```bash
kubectl get secret --all-namespaces -l "owner=helm"
```
```bash
helm uninstall bluegreen
```
```bash
helm package .

export USER=<userXX> (XX = { 01, 12 })
export GC_REGION=europe-west4
export PROJECT=ses-environment

yes | gcloud auth configure-docker ${GC_REGION}-docker.pkg.dev
helm push bluegreendemo-0.0.1.tgz oci://${GC_REGION}-docker.pkg.dev/${PROJECT}/${USER}/
helm install mybluegreendemo oci://${GC_REGION}-docker.pkg.dev/${PROJECT}/${USER}/bluegreendemo --version 0.0.1
helm uninstall mybluegreendemo
```
