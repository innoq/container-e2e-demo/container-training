# From

[Meta Controller](https://metacontroller.github.io/metacontroller/)

## Install MetaController

```bash
# Apply all set of production resources defined in kustomization.yaml in `production` directory .
kubectl apply -k https://github.com/metacontroller/metacontroller/manifests/production

```

## Install Tutorial

see https://metacontroller.github.io/metacontroller/guide/create.html for the steps to follow this exercise.

```bash
kubectl create ns hello
kubectl apply -f crd.yaml
kubectl apply -f controller.yaml
kubectl -n hello create configmap hello-controller --from-file=sync.py
kubectl -n hello apply -f webhook.yaml
kubectl -n hello apply -f audience.yaml
```
