# Config Map

```bash
kubectl create configmap game-config --from-file=./game.properties
```
```bash
kubectl describe configmap game-config
```
```bash
kubectl describe cm game-config
```
```bash
kubectl delete cm game-config
```
```bash
kubectl apply -f pod.yaml
```
```bash
kubectl logs configmap-test-pod | grep ENV
```
```bash
kubectl delete -f pod.yaml
```