# Labels

```bash
kubectl apply -f pods.yaml
```
```bash
kubectl get pods -l environment=production,tier=frontend
```
```bash
kubectl get pods -l 'environment in (staging),tier notin (frontend)'
```
```bash
kubectl delete -f pods.yaml
```