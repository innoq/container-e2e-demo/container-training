# Secret

```bash
kubectl create secret generic db-user-pass --from-file=./username.txt --from-literal=password='S!B\*d$zDsb'
```
```bash
kubectl describe secret db-user-pass
```
```bash
kubectl get secret db-user-pass -o yaml
```
```bash
kubectl get secrets/db-user-pass --template={{.data.password}} | base64 -d
```
```bash
kubectl apply -f pod.yaml
```
```bash
kubectl logs secret-disclosure-pod
```
```bash
kubectl exec -it secret-disclosure-pod -- cat /etc/secrets/password
```
```bash
echo "TEST" | base64
```

**Copy the output into your clipboard and replace the value of key `password` in the following editor. Save the file by pressing `Ctrl + X` and `Y`.**

```bash
KUBE_EDITOR=nano kubectl edit secret db-user-pass
```
```bash
kubectl exec -it secret-disclosure-pod -- cat /etc/secrets/password
```
```bash
kubectl delete -f pod.yaml
```