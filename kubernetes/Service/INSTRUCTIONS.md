# Service

```bash
kubectl apply -f service-a.yaml
```
```bash
kubectl get service service-a-service
```
```bash
kubectl describe service service-a-service
```
```bash
kubectl get services -l run=service-a-service-label -o yaml | grep clusterIP
```
```bash
kubectl run curl --image=radial/busyboxplus:curl -i --rm --tty
  root@container: curl http://<clusterIP>
  root@container: curl http://service-a-service
  root@container: exit
```
```bash
kubectl scale --replicas=0 deploy service-a-deployment
```
```bash
kubectl run curl --image=radial/busyboxplus:curl -i --rm --tty
  root@container: curl http://<clusterIP>
  root@container: exit
```
```bash
kubectl scale --replicas=5 deploy service-a-deployment
```
```bash
kubectl run curl --image=radial/busyboxplus:curl -i --rm --tty
  root@container: curl http://<clusterIP>
  root@container: exit
```
```bash
kubectl scale --replicas=1 deploy service-a-deployment
```
```bash
kubectl delete -f service.yaml
```