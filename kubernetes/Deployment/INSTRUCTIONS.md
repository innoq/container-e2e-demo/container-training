# Deployment

```bash
kubectl apply -f deployment.yaml
```
```bash
kubectl get deployment service-a-deployment
```
```bash
KUBE_EDITOR=nano kubectl edit deployment service-a-deployment
```

**Close nano via Ctrl + X**

```bash
kubectl describe deployment service-a-deployment
```
```bash
kubectl get pods -l run=service-a-pod-label -o yaml | grep -w "podIP"
```
```bash
kubectl scale --replicas=0 deploy service-a-deployment
```
```bash
kubectl get pods -l run=service-a-pod-label -o yaml | grep -w "podIP"
```
```bash
kubectl scale --replicas=5 deploy service-a-deployment
```
```bash
kubectl get pods -l run=service-a-pod-label -o yaml | grep -w "podIP"
```
```bash
kubectl run curl --image=radial/busyboxplus:curl -i --rm --tty
  root@container: curl http://<podIP>
  root@container: exit
```
```bash
kubectl scale --replicas=1 deploy service-a-deployment
```