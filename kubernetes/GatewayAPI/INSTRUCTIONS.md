# Ingress

```bash
kubectl apply -f service-a-route.yaml
kubectl apply -f service-b-route.yaml
kubectl apply -f service-c-route.yaml
```
```bash
kubectl get ingress services-ingress
```
```bash
kubectl describe ingress services-ingress
```

**Open External-IP in your browser: http://*external-IP*/service-a/**

**Open External-IP in your browser: http://*external-IP*/service-b/**

```bash
kubectl delete -f service-a.yaml
kubectl delete -f service-b.yaml
kubectl delete -f service-b-deployment.yaml
kubectl delete -f ingress.yaml
kubectl delete deploy service-a-deployment
```