# Update

**Please open a second terminal window side-by-side with your primary one and enter the following command:**

```bash
watch -n 1 kubectl get pods -o jsonpath="{.items[*].spec.containers[*].image}" -l "run=nginx"
```

**Now continue in the primary one:**

**Deploy 3x nginx 1.16:**

```bash
kubectl apply -f deploy.yaml
```

**Rollout nginx 1.17 by overprovisioning by 1:**

```bash
kubectl apply -f rolling-update.yaml
```
```bash
kubectl rollout status deployment/nginx
```

**Update the image version to nginx 1.18, which triggers rollout:**

**Update the image version to nginx 1.18, which triggers rollout**

```bash
kubectl set image deployment/nginx nginx=nginx:1.18
```
```bash
kubectl rollout status deployment/nginx
```

**Recreate nginx deployment with nginx version 1.19:**

```bash
kubectl apply -f recreate.yaml
```
```bash
watch -n 1 kubectl get pods
```

**Wait until 3 new pods exist**

**In the second terminal window, enter the following command:**

```bash
watch -n 1 kubectl get replicaset
```

```bash
kubectl rollout history deploy/nginx
kubectl rollout undo deployment nginx --to-revision <pick one from above list>
```

```bash
kubectl delete -f deploy.yaml
```

**Deploy “blue” and “green”:**

```bash
kubectl apply -f blue.yaml
```
```bash
kubectl apply -f green.yaml
```
```bash
kubectl get pods
```

**Set Service to “blue”:**

```bash
kubectl apply -f service-blue.yaml
```
```bash
kubectl port-forward svc/bluegreen 8080:80
```

**Set Service to “green”:**

```bash
kubectl apply -f service-green.yaml
```
```bash
kubectl port-forward svc/bluegreen 8080:80
```

**Cleanup**

```bash
kubectl delete -f blue.yaml
kubectl delete -f green.yaml
kubectl delete -f service-green.yaml
```