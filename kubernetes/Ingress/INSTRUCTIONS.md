# Ingress

```bash
kubectl apply -f service-b-deployment.yaml
kubectl apply -f service-a.yaml
kubectl apply -f service-b.yaml
kubectl apply -f service-c.yaml
kubectl apply -f ingress.yaml
```
```bash
kubectl get ingress services-ingress
```
```bash
kubectl describe ingress services-ingress
```

**Open External-IP in your browser: http://*external-IP*/service-a/**

**Open External-IP in your browser: http://*external-IP*/service-b/**

```bash
kubectl delete -f service-a.yaml
kubectl delete -f service-b.yaml
kubectl delete -f ingress.yaml
```