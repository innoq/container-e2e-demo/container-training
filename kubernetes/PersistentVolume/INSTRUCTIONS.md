# Persistent Volume

```bash
kubectl apply -f pvc.yaml
kubectl apply -f pod.yaml
```

**Open a 2nd terminal and run:**

```bash
sleep 10 && kubectl port-forward service-a-with-pv 8080:80
```
```bash
curl http://localhost:8080
```
```bash
kubectl delete -f pod.yaml
```
```bash
kubectl apply -f pod.yaml
```

**Go to 2nd terminal and run again:**

```bash
sleep 10 && kubectl port-forward webserver-with-pv 8080:80
```
```bash
curl http://localhost:8080
```
```bash
kubectl delete -f pod.yaml
kubectl delete -f pvc.yaml
```

**Close the 2nd terminal**
