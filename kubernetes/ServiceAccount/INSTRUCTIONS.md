# Service Account

```bash
kubectl apply -v 6 -f sa.yaml
kubectl apply -f role.yaml
kubectl apply -f .
```
```bash
kubectl exec -it kubernetes-api-inspector-default -- bash
  root@container: export CURL_CA_BUNDLE=/var/run/secrets/kubernetes.io/serviceaccount/ca.crt

  root@container: curl https://kubernetes/api/v1/namespaces/default/pods

  root@container: export TOKEN=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)

  root@container: curl -H "Authorization: Bearer $TOKEN" https://kubernetes/api/v1/namespaces/default/pods

  root@container: exit
```

**Inspect the output**

```bash
kubectl exec -it kubernetes-api-inspector-sa -- bash
  root@container: export CURL_CA_BUNDLE=/var/run/secrets/kubernetes.io/serviceaccount/ca.crt

  root@container: export TOKEN=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)

  root@container: curl -H "Authorization: Bearer $TOKEN" https://kubernetes/api/v1/namespaces/default/pods

  root@container: curl -H "Authorization: Bearer $TOKEN" https://kubernetes/api/v1/namespaces/default/services

  root@container: exit
```

**Inspect the output**

```bash
kubectl delete -f .
```

**Test permission via API call**

```bash
kubectl auth can-i list pods --as system:serviceaccount:default:default
```

```bash
kubectl auth can-i list pods --as system:serviceaccount:default:kubernetes-api-inspector-default
```