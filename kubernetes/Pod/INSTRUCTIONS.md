# Pod

```bash
kubectl apply -f pod.yaml
```
```bash
kubectl get pod webserver-pod
```
```bash
kubectl describe pod webserver-pod
```

**Inspect the object‘s „spec“ and „status“**

```bash
kubectl get pods -l run=webserver-pod-label -o yaml
```
```bash
kubectl get pods -l run=webserver-pod-label -o yaml | grep -w "podIP"
```
```bash
kubectl run curl --image=radial/busyboxplus:curl -i --rm --tty
  root@container: curl http://<podIP>
  root@container: exit
```
```bash
kubectl port-forward webserver-pod 8080:80 &
```
```bash
curl http://localhost:8080
```

**Bring the port forwarding process back to front via `fg` and stop the process via `Ctrl + C`**

```bash
kubectl delete pod webserver-pod
```
```bash
kubectl get pods -l run=webserver-pod-label
```
