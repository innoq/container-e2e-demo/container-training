######
# TAKEN FROM https://github.com/devops-syndicate/k8s-demo
######

## Setup

Run `create-cluster.sh`. After the setup run through, you can access ArgoCD:

- URL: argo-cd.127.0.0.1.nip.io
- username: admin
- password: admin

If that fails, use port forwarding as backup:

Run `kubectl --context kind-argocd-demo port-forward service/argocd-server -n argocd 8080:443` and open the browser at `http://localhost:8080`.

# Deploy application

Copy `application.yaml` and create ArgoCD application from it.

# Stop KinD cluster

```
docker stop argocd-demo-control-plane
```

# Start KinD cluster

```
docker start argocd-demo-control-plane
```

<!-- ```
gcloud source repos clone trainer --project=sebastian-schwaiger
git clone https://github.com/argoproj/argocd-example-apps

cp -rfn argocd-example-apps/helm-guestbook trainer

git commit -m "Helm Demo" -a
git push
``` -->