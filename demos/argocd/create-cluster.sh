#!/bin/bash

# start kind cluster with 3 nodes
kind create cluster --name argocd-demo --config=cluster.yaml

# install ingress controller
kubectl --context kind-argocd-demo apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml

kubectl --context kind-argocd-demo wait --namespace ingress-nginx \
  --for=condition=ready pod \
  --selector=app.kubernetes.io/component=controller \
  --timeout=3m0s

## install argocd
helm --kube-context kind-argocd-demo repo add argo https://argoproj.github.io/argo-helm
helm --kube-context kind-argocd-demo repo update

helm --kube-context kind-argocd-demo upgrade --install \
  argocd argo/argo-cd \
  -n argocd \
  --create-namespace \
  --set global.domain="127.0.0.1.nip.io" \
  --values argocd/helm-values.yaml \
  --timeout 6m0s \
  --wait
