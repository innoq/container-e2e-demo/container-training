# Ingress

```bash
kubectl apply -f deployment.yaml
kubectl apply -f service.yaml
kubectl apply -f ingress.yaml
```

```bash
kubectl sniff -n default $(kubectl get pods -l=run=service-b-pod-label -o jsonpath={..metadata.name})
```

**Determine external LoadBalancer IP:**

```bash
kubectl describe ingress services-ingress
```

```bash
kubectl delete -f .
```