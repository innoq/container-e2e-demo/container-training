# Deployment

This demo is based on https://cloud.google.com/community/tutorials/nginx-ingress-gke.

Install NGINX ingress controller:

```bash
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
helm install nginx-ingress ingress-nginx/ingress-nginx
```

Deploy `Ingress` resource:

```bash
export NGINX_INGRESS_IP=$(kubectl get service nginx-ingress-ingress-nginx-controller -ojson | jq -r '.status.loadBalancer.ingress[].ip')
envsubst < ingress.yaml | kubectl apply -f -
echo "Service A: http://$NGINX_INGRESS_IP.nip.io/service-a"
echo "Service B: http://$NGINX_INGRESS_IP.nip.io/service-b"
```

Deploy the deployments and services:

```bash
kubectl apply -f service-a.yaml
kubectl apply -f service-b.yaml
```

Access the deployments via the browser: http://$NGINX_INGRESS_IP/service-a and http://$NGINX_INGRESS_IP/service-b.

Show the changes made by the control instance watching Kubernetes API object changes:

Open shell in nginx pod and open the following file: `nginx.conf`

Cleanup by uninstalling Helm chart:

```bash
helm uninstall nginx-ingress
```