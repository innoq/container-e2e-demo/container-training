# Bootstrapping

Note: was tested using minikube 1.23.2!

Execute `create-vm.sh` to create a Minikube VM with all required tools installed for the demo.

# Start VM

```bash
minikube start --profile kata
```

# Show the VM's kernel version

```bash
minikube ssh --profile kata -- uname -a
```

# Run a pod that is sharing the kernel with the operating system

## Start pod
```bash
minikube kubectl --profile kata -- apply -f pod-default.yaml
minikube kubectl --profile kata -- describe pod webserver-pod-default
```

## Check kernel version used by pod
```bash
minikube kubectl --profile kata -- exec $(minikube kubectl --profile kata -- get pods -o=name | fgrep webserver-pod-default | sed 's?pod/??') -- uname -a
```

## Shutdown pod
```bash
minikube kubectl --profile kata -- delete -f pod-default.yaml
```

# Run a pod that is isolated using Kata container

## Start isolated pod
```bash
minikube kubectl --profile kata -- apply -f pod-qemu.yaml
minikube kubectl --profile kata -- describe pod webserver-pod-qemu
```

## Check kernel version used by pod
```bash
minikube kubectl --profile kata -- exec $(minikube kubectl --profile kata -- get pods -o=name | fgrep webserver-pod-qemu | sed 's?pod/??') -- uname -a
```

## Shutdown pod
```bash
minikube kubectl --profile kata -- delete -f pod-qemu.yaml
```

# Cleanup

Either stop the VM via

```bash
minikube stop --profile kata
```

or destroy the VM via

```bash
minikube delete --profile kata
```
