#!/bin/bash

minikube start --driver virtualbox --memory 4096 --profile kata --kubernetes-version=v1.18.10 --container-runtime=cri-o 
minikube stop --profile kata

VBoxManage modifyvm "kata" --nested-hw-virt on
minikube start --profile kata

minikube kubectl --profile kata -- apply -f https://raw.githubusercontent.com/kata-containers/kata-containers/main/tools/packaging/kata-deploy/kata-deploy/base/kata-deploy.yaml

CHECK_RESULT=
while [ "${CHECK_RESULT}" == "" ]; do
    POD_NAME=$(minikube kubectl --profile kata -- -n kube-system get pods -o=name | fgrep kata-deploy | sed 's?pod/??')
    CHECK_RESULT=$(minikube kubectl --profile kata -- -n kube-system exec ${POD_NAME} -- ps -ef | fgrep infinity)
    sleep 1
done

minikube kubectl --profile kata -- apply -f https://raw.githubusercontent.com/kata-containers/kata-containers/main/tools/packaging/kata-deploy/runtimeclasses/kata-runtimeClasses.yaml
