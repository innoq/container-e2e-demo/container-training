```
npm ci
pulumi login --local
pulumi config set aws:profile ses-admin
pulumi config set aws:region eu-central-1
```

Passphrase to protect secrets: `1234` for stack `cloudinfra`.

```
export PULUMI_CONFIG_PASSPHRASE=1234
pulumi up
```

To remove the resources, please run 

```
pulumi down
```