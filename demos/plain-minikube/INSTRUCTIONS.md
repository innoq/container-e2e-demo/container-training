# Bootstrapping

Execute `create-vm.sh` to create a Minikube VM with all required tools installed for the demo.

# Start VM

```bash
minikube start --profile plain-k8s
```

# Run `minikube tunnel` (in 2nd terminal) to create a load balancer:
```bash
minikube tunnel --profile plain-k8s
```

# Cleanup

Either stop the VM via

```bash
minikube stop --profile plain-k8s
```

or destroy the VM via

```bash
minikube delete --profile plain-k8s
```
