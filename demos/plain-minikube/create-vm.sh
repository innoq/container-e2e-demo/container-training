#!/bin/bash

# NOTE: this script is based on the command given here: https://istio.io/latest/docs/setup/getting-started/

minikube start --memory=16384 --cpus=4 --kubernetes-version=v1.20.2 --driver=hyperkit --profile plain-k8s
