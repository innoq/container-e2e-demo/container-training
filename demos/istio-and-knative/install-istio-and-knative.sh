#!/bin/bash

# NOTE: this script is based on the command given here: https://istio.io/latest/docs/setup/getting-started/

DIR="istio-1.23.2"
if [ ! -d "$DIR" ]; then
  curl -L https://istio.io/downloadIstio | ISTIO_VERSION=1.23.2 sh -
fi

cd $DIR

./bin/istioctl install --set profile=demo -y

kubectl label namespace default istio-injection=enabled

kubectl apply -f samples/bookinfo/platform/kube/bookinfo.yaml
kubectl apply -f samples/addons

sleep 30
kubectl exec "$(kubectl get pod -l app=ratings -o jsonpath='{.items[0].metadata.name}')" -c ratings -- curl -sS productpage:9080/productpage | grep -o "<title>.*</title>"
kubectl apply -f samples/bookinfo/networking/bookinfo-gateway.yaml

export INGRESS_HOST=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.clusterIP}')
export INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].nodePort}')
export GATEWAY_URL=$INGRESS_HOST:$INGRESS_PORT
echo "Demo application is available at http://$INGRESS_HOST/productpage"

while ! kubectl wait --for=condition=available --timeout=600s deployment/productpage-v1; do sleep 1; done

# NOTE: this script is based on the command given here: https://knative.dev/docs/install/any-kubernetes-cluster/
# NOTE: please prepare environment by installing Istio first!

kubectl apply -f https://github.com/knative/serving/releases/download/knative-v1.16.0/serving-crds.yaml
kubectl apply -f https://github.com/knative/serving/releases/download/knative-v1.16.0/serving-core.yaml
kubectl apply -f https://github.com/knative/net-istio/releases/download/knative-v1.16.0/net-istio.yaml

kubectl label namespace knative-serving istio-injection=enabled

kubectl patch configmap/config-domain \
      --namespace knative-serving \
      --type merge \
      --patch '{"data":{"example.com":""}}'

cd ..

kubectl apply -f service.yaml