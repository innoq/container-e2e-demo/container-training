# Bootstrapping

Execute `install-istio-and-knative.sh` to install Istio and knative in you GKE cluster.


# Get load balancer endpoint and open product page in the browser:
```bash
export INGRESS_HOST=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
echo "Demo application is available at http://$INGRESS_HOST/productpage"
```

# Call the knative service via curl:
```bash
export INGRESS_HOST=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
curl -H "Host: helloworld.default.example.com" "http://$INGRESS_HOST"
```

# Create load on the endpoint to force auto scaling:

```bash
export INGRESS_HOST=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
for i in $(seq 1 1000); do
  echo "."
  curl -H "Host: helloworld.default.example.com" "http://$INGRESS_HOST" &
done
```

# Create load on productpage service
```bash
export INGRESS_HOST=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
for i in $(seq 1 100); do
  echo "."
  curl -s -o /dev/null "http://$INGRESS_HOST/productpage" &
 # sleep 1 
done
```

# Open Kiali dashboard:
```bash
./istio-1.23.2/bin/istioctl dashboard kiali
```

# Open Grafana Metrics Dashboard:
```bash
./istio-1.23.2/bin/istioctl dashboard grafana
```
The Istio dashboard is available at http://localhost:3000/d/G8wLrJIZk/istio-mesh-dashboard?orgId=1&refresh=5s

# Open Jaeger Tracing UI:
```bash
./istio-1.23.2/bin/istioctl dashboard jaeger
```