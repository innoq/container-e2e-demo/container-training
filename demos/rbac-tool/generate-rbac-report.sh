#!/bin/bash

# Note you must have "Dot" installed.
# Instructions for MacOS: brew install graphviz

curl https://raw.githubusercontent.com/alcideio/rbac-tool/master/download.sh | bash

./bin/rbac-tool viz --outformat dot && cat rbac.dot | dot -Tpng -Grankdir=LR > rbac.png  && open rbac.png
