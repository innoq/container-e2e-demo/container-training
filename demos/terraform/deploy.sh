#!/bin/sh

terraform init

terraform plan

terraform apply

export AWS_PROFILE="..."
aws lambda invoke --region=eu-central-1 --function-name=$(terraform output -raw function_name) --no-cli-pager --output json response.json && cat response.json

# uncomment code in hello-world/hello.js

terraform plan

terraform apply -auto-approve

curl "$(terraform output -raw base_url)/hello?Name=Terraform"

terraform destroy