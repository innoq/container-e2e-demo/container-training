Follow along with this [tutorial on HashiCorp Learn](https://learn.hashicorp.com/tutorials/terraform/lambda-api-gateway?in=terraform/aws).

# Initialize workspace

`terraform init`

# Apply to the cloud

`terraform apply`

# Delete resources

`terraform destroy`